#!/bin/bash
export USERNAME="${1:-foo}" # Use the first argument; use `foo` as default if not set.
export USERMAIL="${2:-foo@example.com}" # Use the second argument; use `foo@example.com` as default if not set.

# Create new game.
hurl requests/game_create.hurl

# Add player to game.
hurl requests/player_join_game.hurl --variable playerId="$(hurl requests/player_get.hurl --variable name=$USERNAME --variable mail=$USERMAIL | jq -r '.playerId')" --variable  gameId="$(hurl requests/game_get_all.hurl | jq -r '.[0].gameId')"

# Start the game.
hurl requests/game_start.hurl --variable gameId="$(hurl requests/game_get_all.hurl | jq -r '.[0].gameId')"
