#!/usr/bin/env bash

docker compose config --services | while read -r svc; do docker compose logs --no-color --no-log-prefix --since 24h "$svc" >"logs/$svc.txt"; done
echo -n "" >"logs/images.txt"
docker compose config --images | while read -r img; do docker image inspect "$img" >>"logs/images.txt"; done
docker compose config --resolve-image-digests >"logs/docker-compose.yaml"

zip -r "logs-$(date +"%Y-%m-%d-%H-%M-%S").zip" logs
