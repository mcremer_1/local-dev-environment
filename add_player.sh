#!/bin/bash
export USERNAME="${1:-foo}" # Use the first argument; use `foo` as default if not set.
export USERMAIL="${2:-foo@example.com}" # Use the second argument; use `foo@example.com` as default if not set.
hurl requests/player_create.hurl --variable name=$USERNAME --variable email=$USERMAIL
