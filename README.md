# Local Dev Environment

The local-dev-env script will provide you with a working environment for development on your machine.
It will automatically spawn corresponding docker containers and map ports on your machine.
In addition it contains some [hurl] request files for testing purposes.

## Prerequisties
* Docker and Docker Compose
* JDK 17
* [hurl](https://github.com/Orange-OpenSource/hurl) if you want to use provided requests (See [Using hurl-Requests](#using-hurl-Requests))

## Usage

⚠️ **Note**
If you have any problems using the tool, checkout the [Troubleshooting](#troubleshooting) section.

### Clone

The repository consists of git submodules that need to be initialized first.

```
git clone https://gitlab.com/the-microservice-dungeon/local-dev-environment.git
cd local-dev-environment
```

### Authenticate to GitLab Registry

Please consult the [GitLab documentation](https://docs.gitlab.com/ee/user/packages/container_registry/#authenticate-with-the-container-registry) for details.
You will need to generate a Personal Access Token (PAT) and then authenticate with the gitlab registry:
```
docker login registry.gitlab.com -u "<User>" -p "<PAT>"
```

### Usage

Here are some usage examples:
```
# Create and Start Services
docker compose up [services]

# Start and Stop
docker compose stop [services]
docker compose start [services]

# Start everything except map (Possibly works on *nix only)
docker compose start $(docker compose config --services | grep -v map)
docker compose config --services | grep -v map | xargs docker compose start

# Update Services
docker compose pull [services]

# Remove Services
docker compose down [services]
```
Available Services can be used from [Reference](#reference). Please consult the [Compose CLI Reference](https://docs.docker.com/compose/reference/) for more informations

## Watch Mode

If you are on a linux machine you can utilize watchtower to keep your images up-to-date while running.
To use it, run:
It should just update the running containers from the compose file, but thais behaviour is not heavily tested. It might be the case, that watchtower will monitor **all** containers by default, not just the running containers from the compose file. Keep that in mind when using this way.
```sh
docker compose -f docker-compose.yaml -f docker-compose.watch.yaml up
```

## Reference

| Profile        | Description                                               |
| -------------- | --------------------------------------------------------- |
| Infrastructure | Infrastrucutral dependencies like Kafka and Databases     |
| Services       | Core services of the microservice dungeon like game       |

The following Ports will be mapped to your machine:
| Port  | Service                       |
| ----- | ----------------------------- |
| 3306  | MySQL                         |
| 5432  | Postgres                      |
| 9092  | Kafka (Internal)              |
| 29092 | Kafka (Outside)               |
| 5672  | RabbitMQ (AQMP)               |
| 15672 | RabbitMQ (Management Console) |
| 9000  | RedPanda Console              |
| 8080  | Game                          |
| 8081  | Map                           |
| 8082  | Robot                         |
| 8083  | Trading                       |
| 8084  | Gamelog                       |

The following credentials are used:

| Service  | Username | Password |
|----------|----------|----------|
| Postgres | postgres | root     |
| MySQL    | root     | root     |
| RabbitMQ | admin    | admin    |

## Using hurl-Requests

The provided Hurl requests are at the moment experimental and may change over time.
They are not considered to be stable.
We might make them more conveniently usable in the future.
The *.hurl files **SHOULD** be self-explanatory if you are familar with HTTP.
Also please visit the [offical documentation](https://hurl.dev/) for more details.

| Use Case                                  | Command                                                                                           |
|-------------------------------------------|---------------------------------------------------------------------------------------------------|
| **List all games**                        | `hurl requests/game_get_all.hurl`                                                                 |
| **Create a new game**                     | `hurl requests/game_create.hurl`                                                                  |
| **Create a new game with 15s round time** | `hurl requests/game_create_short.hurl`                                                            |
| **Start a game**                          | `hurl requests/game_start.hurl --variable gameId=<Game ID>`                                       |
| **End a game**                            | `hurl requests/game_end.hurl --variable gameId=<Game ID>`                                         |
| **Set round duration to 15s**             | `hurl requests/game_duration.hurl`                                                                |
| **Create a new player**                   | `hurl requests/player_create.hurl --variable name=<Player Name> --variable email=<Player Email>`  |
| **Get the details of a player**           | `hurl requests/player_get.hurl --variable name=<Player Name> --variable mail=<Player E-Mail>`     |
| **Register a player to a game**           | `hurl requests/player_join_game.hurl --variable gameId=<Game ID> --variable playerId=<Player ID>` |

### Useful snippets for *nix:
**Start / End the most recent created game**
```sh
hurl requests/game_start.hurl --variable gameId=$(hurl requests/game_get_all.hurl | jq -r '.[0].gameId')
hurl requests/game_end.hurl --variable gameId=$(hurl requests/game_get_all.hurl | jq -r '.[0].gameId')
```

### Quick player creation and game start
The following scripts are mainly useful for the development of the gamelog.
They have been tested on macOS and should work on most Unix-Systems too.

**Quickly create a player**
```sh
./add_player.sh %player-name %player-email
```
Replace `%player-name` with your player name and `%player-email` with your player email-address.
Player name and email will default to `foo` and `foo@example.com` if no arguments are provided.

**Quickly create a game, join a player and start the game**
```sh
./create_game_and_join.sh %player-name %player-email
```
Replace `%player-name` with your player name and `%player-email` with your player email-address.
Player name and email will default to `foo` and `foo@example.com` if no arguments are provided.
The player has to exist already to be able to join, so run the `add_player` script to create a player.

## Troubleshooting

### Generate Log Files

The repository contains a simple script to export logs in a transferable manner.
For this purpose the `export-logs` scripts can be used.
It is provided as a bash and powershell script file.
Calling the script by using `./export-logs.sh` / `./export-logs.ps1` will export the most recent logs into the `logs/`
directory and archives them into a zip-file in the main directory.
This log archives are meant to be transferable for debugging purposes.

### The container name is already in use by container

The script will try to create docker containers with common names in the microservice dungeon. So it might be the case that you already have existing containers with the name.
If that is the case, the script will not be able to create new containers and you might have troubles developing. Therefore it might be best to remove all those containers before.

You can check if that's the case by running one of the following scripts and removing corresponding containers.
**PowerShell**
```pwsh
$containers = @("kafka", "console", "postgres", "mysql", "map", "robot", "game", "trading", "gamelog")

# If this command gives you any output, you have already have containers with that name running
&docker container ls -a ($containers | % { "--filter", "name=^$_`$" })

# ⚠️ WARNING: THIS IS A DESTRUCTIVE OPERATION. CONTAINERS WILL BE REMOVED.
&docker stop $containers
&docker rm $containers
```

**Bash**
```pwsh
containers=("kafka" "console" "postgres" "mysql" "map" "robot" "game" "trading" "gamelog")

# If this command gives you any output, you have already have containers with that name running
printf "--filter name=^%s\$\n" "${containers[@]}" | xargs docker container ls -a

# ⚠️ WARNING: THIS IS A DESTRUCTIVE OPERATION. CONTAINERS WILL BE REMOVED.
docker stop $containers
docker rm $containers
```

### Bind: address already in use

Compose will try to create port mappings like described in the [Refernce](#reference). Make sure that nothing is running on the port.
