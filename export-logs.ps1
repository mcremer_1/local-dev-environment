docker compose config --services | ForEach-Object {
    $svc = $_
    docker compose logs --no-color --no-log-prefix --since 24h "$svc" | Out-File "logs/$svc.txt"
}

Clear-Content "logs/images.txt"
docker compose config --images | ForEach-Object {
    $img = $_
    docker image inspect "$img" | Add-Content "logs/images.txt"
}

docker compose config --resolve-image-digests | Out-File "logs/docker-compose.yaml"

Compress-Archive -Path logs -DestinationPath "logs-$(Get-Date -Format "yyy-MM-dd-HH-mm-ss").zip"
